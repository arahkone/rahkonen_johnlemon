﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Observer : MonoBehaviour
{
    public Transform player;
    public GameEnding gameEnding;

    public bool isGargoyle;

    bool m_IsPlayerInRange;

    bool dead;

  

    void OnTriggerEnter(Collider other)
    {
        if (other.transform == player)
        {
            m_IsPlayerInRange = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.transform == player)
        {
            m_IsPlayerInRange = false;
        }
    }

    void Update()
    {
        if (dead)
            return;


        if (m_IsPlayerInRange)
        {
            Vector3 direction = player.position - transform.position + Vector3.up;
            Ray ray = new Ray(transform.position, direction);
            RaycastHit raycastHit;

            if(Physics.Raycast(ray, out raycastHit))
            {
                if (raycastHit.collider.transform == player)
                {

                    if (!dead) {
                        print("I called caught: " + transform.parent.gameObject.name);

                        if (isGargoyle) {
                            gameEnding.raiseBravery();
                        }
                        else 
                        gameEnding.CaughtPlayer();
                        Destroy(transform.parent.gameObject);
                        dead = true;
                    }
                }
                
            } 
        }
    }

}
