﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameEnding : MonoBehaviour
{
    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    public GameObject player;
    public CanvasGroup exitBackgroundImageCanvasGroup;
    public AudioSource exitAudio;
    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public AudioSource caughtAudio;
    public Text countText;
    public Text peeingText;
    public Text cryingText;


    private int count;

    bool m_IsPlayerAtExit;
    bool m_IsPlayerCaught;
    float m_Timer;
    bool m_HasAudioPlayed;



    private void Start()
    {
        count = 5;
        SetCountText();

    }

    
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            m_IsPlayerAtExit = true;
            //*count = count - 1;
            //*SetCountText();
        }
    }
    public void raiseBravery()
    {
        count = count + 1;
        SetCountText();
    }
    public void CaughtPlayer()
    {
        print("Caught~");
        count = count - 1;
        SetCountText();
        if (count <= 0)
        m_IsPlayerCaught = true;
    }
     
    void Update()
    {
        if (m_IsPlayerAtExit)
        {
            EndLevel(exitBackgroundImageCanvasGroup, false, exitAudio);
        }
        else if (m_IsPlayerCaught)
        {
            EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);

            

        }
    }
    void EndLevel (CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
    {

        if (!m_HasAudioPlayed)
        {
            audioSource.Play();
            m_HasAudioPlayed = true;
        }

        m_Timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_Timer / fadeDuration;

       

        if(m_Timer > fadeDuration + displayImageDuration)
        {
            if (doRestart)
            {
                SceneManager.LoadScene(0);
            }
            else
            {
                Application.Quit();
            }
        }
    }

    void SetCountText()
    {
        countText.text = "Bravery:" + count.ToString();
        if (count <= 0)
        {
            EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
        }
        if (count ==3) {
            cryingText.text = "You are crying from fear!";
        }
        if (count ==1)
        {
            peeingText.text = "You just peed your pants from sheer fear!";
        }
    }
}
